//'use strict';
function cleanUp(str) {
	console.log(str);
	str = str.replace(/\s+/g, " ");
	return str;
};

var app = angular.module('Ipadque', ['ngAnimate','ui.bootstrap']);

app.filter("addZeros",function(){
	return function(n, length){
		var str = (n > 0 ? n : -n) + "";
    	var zeros = "";
    	for (var i = length - str.length; i > 0; i--)
        	zeros += "0";
    		zeros += str;
    	return n >= 0 ? zeros : "-" + zeros;

	}
});

app.controller('IpadqueController',['$scope','$rootScope','$route', '$compile', '$window', '$uibModal','$log', '$routeParams', '$location', 'generalFactory', 'socket',
	function($scope, $rootScope, $route, $compile, $window, $uibModal, $log, $routeParams, $location, generalFactory, socket){

	//session_check();

	$scope.session_check = function(){
		generalFactory.getHandler('/login/ioCheck', function(response){
			console.log(response.iologgedin);
			if(response.iologgedin == false){
				//if($window.navigator.userAgent.match(/iPad/i)){
					$location.path('/ipadque');
				//}else{
					//alert('Please open this page on an IPAD. Thank you!');
					//$location.path('/');
				//}
			}
			// else{
			// 	console.log(response.iologgedin);
			// 	$location.path('/iologin/ipad');
			// }
		});
	};

	$scope.init = function(){
		generalFactory.getHandler('/main/getServices', function(response){
			if(response.status === "SUCCESS"){
				$scope.buttons = response.result;
			}
		});
	}

	//disabled function 2/12/18

	// $scope.input = function(service){
	// 	if($scope.firstname == undefined){$scope.firstname = '';}
	// 	if($scope.lastname == undefined){$scope.lastname = '';}
	// 	if($scope.firstname != '' && $scope.lastname != '' && $scope.salutation != ''){
	// 		if(service === 3){
	// 			$scope.openModal('', 'partials/form_ar.html', '', '');
	// 		}else{
	// 			$scope.openModal('', 'partials/form_inputAlias.html', service, 'N/A');
	// 		}
	// 	}else{
	// 		alert('Please input your name properly and select the proper salutation. Thank you!');
	// 	}
	// };

	$scope.input = function(service){
		if($scope.quecard == undefined){$scope.quecard = '';}
		if(Number($scope.quecard.length) > 2){
			if(service === 3){
				$scope.openModal('', 'partials/form_ar.html', '', '');
			}else{
				$scope.openModal('', 'partials/form_inputAlias.html', service, 'N/A');
			}
		}else if(Number($scope.quecard.length) < 9 && Number($scope.quecard.length) > 0){
			alert("You've entered an invalid number. Please check and enter your queue card number correctly.");
		}
		else{
			alert('Please enter correct queue card number. Thank you!');
		}
	};


	$scope.openModal = function (size, url, service, ar) {
		var modalInstanceIpad = $uibModal.open({
	      	animation: true,
	      	templateUrl: url,
	      	controller: 'ModalInstanceCtrlIpad',
	      	size: size,
	      	scope: $scope,
	      	windowClass: 'app-modal-window',
	      	resolve: {
	        		service: function () {
	          			return service;
	        		},
	        		ar: function () {
	        			return ar;
	        		}
	     	}
	    });
	    modalInstanceIpad.result.then(function (selectedItem) {
	      	$scope.selected = selectedItem;
	    }, function () {
	      	$log.info('Modal dismissed at: ' + new Date());
	    });
	};
}]);


app.controller('ModalInstanceCtrlIpad' , ['$scope', '$route', '$uibModalInstance', '$location' , 'service', 'ar','generalFactory','socket',
	function ( $scope, $route, $uibModalInstance, $location, service, ar, generalFactory, socket ) {
	$scope.ar = $scope.quecard;
	// var firstname = cleanUp($scope.firstname);
	// var lastname = cleanUp($scope.lastname);
	// $scope.alias1 = firstname.toUpperCase() + ' ' + lastname.charAt(0).toUpperCase()+'.';
	// $scope.alias2 = firstname.charAt(0).toUpperCase() + '. ' + lastname.toUpperCase();
	// $scope.alias3 = firstname.toUpperCase() + ' ' + lastname.toUpperCase();
	$scope.inputar = function(){
	  	if($scope.ar == undefined){$scope.ar = '';}
	  	if($scope.ar != ''){
	  		$uibModalInstance.dismiss('cancel');
	  		$scope.$parent.openModal('', 'partials/form_inputAlias.html', 3, $scope.ar);
	  	}else{
	  		alert('Please input your Acknowledgement Reciept Number first. Thank you!');
	  	}
	};

	$scope.inputque = function(){
			// var alias = cleanUp($scope.alias);
			generalFactory.postHandler('/ipadque/service',
			{status: service, salutation: "", firstname: "", lastname: "" , ar: ar, alias: "", quecard: $scope.quecard},
			function(response){

					if(response.success == 'ERROR'){
						$scope.message = 'Error on data save';
					} else {
						socket.emit('newPendingRequest',response.numberque);
						$scope.message = response.success;
						$route.reload();
					}
			});
		
	};

	$scope.cancel = function () {
	   	$uibModalInstance.dismiss('cancel');
	};
}]);



app.directive('arf', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
        	var regex = new RegExp("^[a-zA-Z0-9._\b]+$");
		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

		    	if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }

        });
    };
});
app.directive('fname', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
        	var regex = new RegExp("^[a-zA-Z._\b]+$");
		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

		    	if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }

        });
    };
});
app.directive('lname', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
        	var regex = new RegExp("^[a-zA-Z._\b]+$");
		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

		    	if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }

        });
    };
});

// app.directive('alias', function () {
//     return function (scope, element, attrs) {
//         element.bind("keydown keypress", function (event) {
//         	var regex = new RegExp("^[a-zA-Z._\b]+$");
// 		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

// 		    	if (!regex.test(key)) {
// 			       event.preventDefault();
// 			       return false;
// 			    }

//         });
//     };
// });
